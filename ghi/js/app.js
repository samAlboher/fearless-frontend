function createCard(name, location, description, pictureUrl,startDate, endDate) {
    return `
    <div class="col-sm-6 col-md-4 mb-1">
    <div class="shadow-1g p-3 mb-5 bg-body rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">${startDate}-${endDate}</div>
      </div>
      </div>
    `;
  }
function catastrophicError(e){
    return `<div class="alert alert-primary" role="alert">
   ERROR: You're machine will implode in 60 seconds
    </div>`
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toLocaleDateString();
            const endDate = new Date(details.conference.ends).toLocaleDateString();
            const html = createCard(name, location, description, pictureUrl, startDate, endDate);
            // console.log(html);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      const html = catastrophicError(e);
      const error = document.querySelector('.row');
      error.innerHTML += html;
    //   console.error(e);
    }

  });


        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;

        // const detailUrl = `http://localhost:8000${conference.href}`;
        // const detailResponse = await fetch (detailUrl);
        // if (detailResponse.ok) {
        //     const details = await detailResponse.json();
        //     const detailTag = document.querySelector('.card-text');
        //     detailTag.innerHTML = details.conference.description;

        //     const imgTag = document.querySelector('.card-img-top')
        //     imgTag.src = details.conference.location.picture_url;
        //     console.log(details);


// const myList = document.querySelector('ul');
// const myRequest = new Request('products.json');

// fetch(myRequest)
//   .then((response) => response.json())
//   .then((data) => {
//     for (const product of data.products) {
//       const listItem = document.createElement('li');
//       listItem.appendChild(
//         document.createElement('strong')
//       ).textContent = product.Name;
//       listItem.append(
//         ` can be found in ${
//           product.Location
//         }. Cost: `
//       );
//       listItem.appendChild(
//         document.createElement('strong')
//       ).textContent = `£${product.Price}`;
//       myList.appendChild(listItem);
//     }
//   })
//   .catch(console.error);
