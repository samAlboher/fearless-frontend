window.addEventListener('DOMContentLoaded', async () => {
    const url = `http://localhost:8000/api/conferences/`
    const response = await fetch(url);
    const formTag = document.getElementById('create-presentation-form');
    console.log("this is the from tag", formTag)
    formTag.addEventListener('submit', async event =>{
        event.preventDefault()
        const formData = new FormData(formTag)
        console.log('this is the formData')
        console.log(formData)
        for (let obj of formData){
            console.log(obj)
        }
        const json = JSON.stringify(Object.fromEntries(formData))
        console.log('this is json',json)
        const conferenceData = JSON.parse(json);
        // const conferenceData = Object.fromEntries(formData)
        // console.log("this is conferenceData", conferenceData)
        // create a string of an object containing formData elements(which are a list)
        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceData.conference}/presentations/`
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newPresentation = await response.json()
            // console.log(newPresentation)
        }
    })
    if (response.ok){
        const data = await response.json();
        // console.log(data)
        const selectTag = document.getElementById('conferences');
        for ( let conference of data.conferences){
            const option = document.createElement('option');
            option.value = conference.id
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }
    }


})


// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/'

//     try {
//         const response = await fetch(url);

//         if (!response.ok) {
//             console.error("there has been an error")
//         } else {
//             const data = await response.json();
//             const selectTag = document.getElementById('conferences');

//             for (let conference of data.conferences) {
//                 let option = document.createElement('option');
//                 option.value = conference.id;
//                 option.innerHTML = conference.name;
//                 selectTag.appendChild(option);
//             }

//             const formTag = document.getElementById('create-presentation-form');
//             formTag.addEventListener('submit', async event => {
//                 event.preventDefault();
//                 const formData = new FormData(formTag);
//                 const conferenceid = Object.fromEntries(formData).conferences;

//                 const presenter_name = Object.fromEntries(formData).presenter_name;
//                 const presenter_email = Object.fromEntries(formData).presenter_email;
//                 const synopsis = Object.fromEntries(formData).synopsis;
//                 const company_name = Object.fromEntries(formData).company_name;
//                 const title = Object.fromEntries(formData).title;
//                 let test = {}
//                 test["presenter_name"] = presenter_name
//                 test["presenter_email"] = presenter_email
//                 test["synopsis"] = synopsis
//                 test["company_name"] = company_name
//                 if (title === {}) {

//                 } else {
//                     test["title"] = title
//                 }
//                 let json = JSON.stringify(test)

//                 const presentationsUrl = `http://localhost:8000/api/conferences/${conferenceid}/presentations/`;
//                 const fetchConfig = {
//                     method: "post",
//                     body: json,
//                     headers: {
//                         'Content-Type': 'application/json',
//                     },
//                 };
//                 const response = await fetch(presentationsUrl, fetchConfig);
//                 if (response.ok) {
//                     formTag.reset();
//                     const newPresentation = await response.json()
//                     console.log(newPresentation)
//                 }
//             });
//         }
//     } catch (e) {
//         console.error("There has been an error")
//     }
// });


// window.addEventListener('DOMContentLoaded', async () =>{
//     const conferenceRequestUrl = 'http://localhost:8000/api/conferences'
//     const conferenceRequestResponse = await fetch (conferenceRequestUrl);
//     if (!conferenceRequestResponse.ok){
//         throw " Invalid Request was made fetching Conference list";
//     }else{
//         const conferenceList = await conferenceRequestResponse.json()
//         const conferenceListFormatted = []
//         for (let i = 0; i < conferenceList['conferences'].length; i++){
//             let conferenceListSplit = conferenceList['conferences'][i].href.split('/')
//             let conferenceId = conferenceListSplit[conferenceListSplit.length-2]
//             conferenceListFormatted.push(`<option value ="${parseInt(conferenceId)}">${conferenceList['conferences'][i].name}</option>`)
//         }
//         const conferenceListHtml = `
//         <option selected value="">Choose a Conference</option>${conferenceListFormatted.join('')}`
//         const conferenceTag = document.querySelector('.form-select')
//     }
//     const formTag = document.getElementById('create-conference-form');
//     formTag.addEventListener('submit', async event =>{
//         event.preventDefault();
//         const formData = new FormData(formTag);
//         const json = JSON.stringify(Object.fromEntries(formData));
//         const jsondata = JSON.parse(json)
//         const presentationUrl = `http://localhost:8000/api/conferences/${jsondata.conference}/presentations/`;
//         const fetchConfig = {
//             method : "post",
//             body : json,
//             header: {
//                 'Content-type': 'application/json'
//             }
//         }
//         const response = await fetch(presentationUrl, fetchConfig);
//         if (response.ok){
//             formTag.reset();
//             const newPresentation = await response.json;
//         }
//     })
// });
